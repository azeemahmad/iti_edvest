<?php

namespace App\Http\Controllers\Client\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('client_guest');
    }


    protected function guard()
    {
        return Auth::guard('client');
    }

    public function broker()
    {
        return Password::broker('clients');
    }

    public function showResetForm(Request $request, $token = null)
    {
        $user = DB::table('clients_password_resets')->get();
        $email='';
        if(isset($user) && $user->isNotEmpty()){
            foreach($user as $use){
                if(Hash::check($token, $use->token)){
                    $email=$use->email;

                }
            }
        }
        return view('client.auth.passwords.reset')->with(
            ['token' => $token, 'email' => $email]
        );
    }
}
