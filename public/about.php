<html>
<head>
	<title>About | ITI EdVest</title>
	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	
	<div class="fluid-container inner about-page">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-6 padding-zero image-outer float-left">
						<img src="images/image.jpg" style="width:100%;">
					</div>

					<div class="col-md-6 text">
						<?php include_once('includes/header.php'); ?>
						<!-- <h3> <a href="#"><img class="logo_inner" src="images/home_icons/logo.png"></a> -->

						<p class="right-text">
							<h3 class="pull-left">About</h3>
							<hr>
							ITI EdVest is an edu-focused financing initiative backed by The Investment Trust Of India Limited (<a href="https://itigroup.co.in/" target="_blank">www.itigroup.co.in</a>). With an objective to support the entire ecosystem in the education sector, we have developed products and solutions that suit the strategic objectives of our partners. Our expertise lies in deciphering the complexities experienced by education institutions and curating niche solutions.
						<br><br>
						 We believe that each customer is our partner and we work with a single point agenda of catalyzing growth for education institutions. Our understanding of the entire education sector value chain and our commitment reflects in the quality of the work we do. 
						 <br><br>
						 The Investment Trust Of India Limited (<a href="https://itigroup.co.in/" target="_blank">www.itigroup.co.in</a>), a public limited company listed on the Bombay Stock Exchange, is promoted by Mr. Sudhir Valia, Executive Director of Sun Pharmaceuticals.</p>						 
					</div>					
				</div>
				<div class="clearfix"></div>
				<?php include_once('includes/footer.php'); ?>		
			</div>
		</div>
	</div>

	<script src="inner.js"></script>

</body>
</html>

