<html>
<head>
	<title>IRM in India| ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>

	<style type="text/css">
		.founding-team .text h5{
			border-bottom: 1px solid #17375e;
		    width: max-content;
		    padding-bottom: 2px;
		}
		.founding-team .logo_inner{
			    max-width: 12%;
		}
		.founding-team .our-mentors{
			border-top: 2px solid #333333;
		    padding: 15px 0px 5px 0px;
		    margin-top: 25px;
		}
		.founding-team .outer-mentors{
			padding-bottom: 20px;
		}
		@media only screen and (max-device-width: 1600px) and (max-device-height: 900px){
			footer {
			    bottom: 14px;
			}
		}
	</style>

</head>
<body>
	
	<div class="fluid-container inner ">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right irm_block">
				<div class="col-md-12 padding-zero">
					<div class="col-md-12 padding-zero image-outer  text">
						
						<?php include_once('includes/header.php'); ?>

						<p class="outer">
						<h3 class="pull-left">IRM in India</h3>
							<hr>

							
							<div class="col-md-12 col-sm-12 col-xs-12">
								
								<!-- <img src="images/irm/1.jpg" class="img_irm" style="float:right"> -->
								  <p>ITI EdVest - the education initiative of The Investment Trust of India Limited, is the accredited partner of The Institute of Risk Management for promoting and delivering risk management education in India.										
									</p>
									<p>
										ITI EdVest is proud to offer the following for the Indian region:
									</p>

			          			 	<ul class="list_irm">
			          			 		<li>Customised training programs for multinationals, Indian corporates and SMEs</li>
										<li>Young Leaders Program on Business Risk Management, based on IRM's flagship Fundamentals of Risk Management (FoRM) course, for undergraduate/postgraduate students, those with a family business/entrepreneurship background and young working professionals</li>
										<li>Other IRM programmes</li>
			          			 	</ul>
							</div>


						<h5>Sanjay Himatsingani, Director of Training & Business Development IRM said:</h5>	
						<hr>
						<p>
							"We are very pleased about this initiative with ITI EdVest in India. Our training and education offer is well respected and attracts students from a wide variety of organisations from all over the world.	
							We are delighted that students in India will have the opportunity to increase their risk management knowledge, accessing our courses locally through ITI EdVest."
						</p>

						<h5>Hersh Shah, Founder & Head, ITI EdVest said:</h5>
						<hr>

						<p>
							"We are very excited about this potential collaboration as we embark on a journey to transform the landscape of risk management in India through our proprietary simulation pedagogy which will benefit entrepreneurs, professionals and organisations across India."

						</p>	
						<p>To affiliate / partner with IRM, contact ITI EdVest at: <a href="tel:02224391266">+91-22-24391266</a> or write to <a href="mailto:irm.edvest@itiorg.com">irm.edvest@itiorg.com</a></p>
			            
						<div class="clearfix"></div>

							
							<h5 class="title_tagline">Young Leaders Business Risk</h5>
							

							<img src="images/irm/2.jpg" class="img_irm" style="float:right">
							<p>This practical introductory two day course in enterprise risk management will allow you to learn tools and techniques that can be immediately applied at work. </p>
							<p>
								It covers the fundamentals of enterprise risk management in a dynamic and interactive learning environment, using the theory and practice of risk management in-line with international standards and industry best practice.
							</p>
							<p>
								We are offering this program to selected education institutions in India. If you wish to run or attend this course at your institute, please contact us at:
							</p>

							<p><a href="tel:02224391266">+91-22-24391266</a> or write to <a href="mailto:irm.edvest@itiorg.com">irm.edvest@itiorg.com</a></p>

							<p class="link"><a href="docs/ITI-brochure-corporate_final.pdf" target="_blank">For more information download the full brochure here >></a></p>

						
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>


</body>
</html>
