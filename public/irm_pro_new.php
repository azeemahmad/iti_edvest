<html>
<head>
	<title>IRM Open Programs| ITI EdVest</title>
	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


 

	<style type="text/css">
		img{
			max-width: 100%;
		}
		.no_padding{
			padding: 0px;
		}
		.reg_btn{
			background: #dd6f56;
    		padding: 5px 20px!important;   
    		/*color:#fff; 		*/
		}
		.container.pro_logo{
			display: none;
		}

	</style>
</head>
<body class="irm_pro_new">
	<div class="container pro_logo">
		<?php include_once('includes/header.php'); ?>
	</div>
	
	<section class="top_nav">
		<div class="container-fluid">
			<div class="col-md-12 col-sm-12 col-xs-12">
			
				
				<ul class="top_nav_list">
					<li class="dwnld"><a href="docs/ITI-brochure-corporate_final.pdf" target="_blank" class="reg_btn left_btn"><i class="fa fa-hand-o-down" aria-hidden="true"></i>  Download Brochure</a></li>
					<li><a href="tel:02224391266"><i class="fa fa-phone" aria-hidden="true"></i> +91-22-24391266</a></li>
					<li><a href="irm.edvest@itiorg.com"><i class="fa fa-envelope" aria-hidden="true"></i></i> irm.edvest@itiorg.com</a></li>
					<li><a href="form_irm.php" target="_blank" class="reg_btn">Registration Form</a></li>
				</ul>
			</div>
		</div>
	</section>
	<section class="banner_section">
		<div class="container-fluid no_padding">
			<div class="col-md-12 no_padding">
				<img src="images/irm/banner_pro.jpg" class="img-responsive">
			</div>			
		</div>
	</section>
	<section class="irm_about">
		<div class="container tri">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
			<h2 class="til_pro">Research</h2>
			</div>
			<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="text-center">				
					<p>
						A study reveals that <strong>75%</strong> of the companies that existed in <strong>1950</strong> will no longer exist as on <strong>2025</strong> - this highlights the increasing role of understanding risk management from a business perspective. IRM's Young Leaders Program on Business Risk Management is a two day practical and simulation based course in enterprise risk management that will allow you to learn tools and techniques that can be immediately applied at work, in family business or in your startup. It covers the fundamentals of enterprise risk management in a dynamic and interactive learning environment, using the theory and practice of risk management in-line with international standards and industry best practices. Risk is no longer about insurance, it's about taking decisions, mitigation strategies and role of stakeholders in managing risks. Candidates, upon completing the assessment test, would be awarded with a Certificate from IRM London.
					</p>	
				</div>
			</div>
		</div>
	</section>

	<section class=" about_london">
		<div class="container tri">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
			<h2 class="til_pro">About IRM London</h2>
			</div>
			<div class="col-md-12 col-xs-12 col-sm-12">
				<div class="text-center">				
					<p>
						IRM is the leading global body for professional risk management. We help build excellence in risk management to improve the way organisations work. We provide globally recognised qualifications and training, publish research and guidance and set professional standards. IRM members work in many roles, in all industries and across the public, private and not for profit sectors across the world. IRM recently published a research with University of Cambridge (Judge Business School) on Risk Management Perspectives of Global Corporations. For more information, you may visit - <a href="https://www.theirm.org/" target="_blank">https://www.theirm.org/</a>
					</p>	
				</div>
			</div>
		</div>
	</section>

	<section class="section_inner section_batch">
		<div class="container">
			<div class="col-md-12 col-sm-12 col-xs-12 text-center">
				<h2 class="til_pro">Upcoming Batches</h2>
			</div>
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="event_block">
					<div class="date">
						<i class="fa fa-calendar-alt calener_icon"></i>  9th and 10th May 2019  <span class="full_day">Full days</span>
					</div>
					<div class="patch">
						<p><span class="seats">30 seats</span> Registration on first come first serve</p> 
						<p>INR 16,500 plus taxes per student [lunch, refreshments and snacks included]</p>
					</div>
				
					<div class="event_footer">
						<p>Board Room at ITI EdVest Corporate Office</p>
					</div>
				</div>

				<div class="event_block">
					<div class="date">
						<i class="fa fa-calendar-alt calener_icon"></i>  18th and 19th May 2019  <span class="full_day">Full days</span>
					</div>
					<div class="patch">
						<p><span class="seats">30 seats</span> Registration on first come first serve</p> 
						<p>INR 16,500 plus taxes per student [lunch, refreshments and snacks included]</p>
					</div>
					
					<div class="event_footer">
						<p>Board Room at ITI EdVest Corporate Office</p>
					</div>
				</div>
			</div>			
		</div>
	</section>
	

	<?php include_once('includes/footer.php'); ?>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>	
	<script src="inner.js"></script>

	<script type="text/javascript">
		$(window).scroll(function () {
		    var dist = $(window).scrollTop()
		    if (dist > 100) {
		        $(".top_nav").addClass("fixed_nav")
		    } else {
		        $(".top_nav").removeClass("fixed_nav")
		    }
		});
	</script>



</body>
</html>
