<html>
<head>
	<title>IRM Open Programs| ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>

	<style type="text/css">
		.founding-team .text h5{
			border-bottom: 1px solid #17375e;
		    width: max-content;
		    padding-bottom: 2px;
		}
		.founding-team .logo_inner{
			    max-width: 12%;
		}
		.founding-team .our-mentors{
			border-top: 2px solid #333333;
		    padding: 15px 0px 5px 0px;
		    margin-top: 25px;
		}
		.founding-team .outer-mentors{
			padding-bottom: 20px;
		}
		
		@media only screen and (max-device-width: 1600px) and (max-device-height: 900px){
			footer {
			    bottom: 14px;
			}
		}
	</style>

</head>
<body>
	
	<div class="fluid-container inner ">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right irm_block">
				<div class="col-md-12 padding-zero">
					<div class="col-md-12 padding-zero image-outer  text">
						
						<?php include_once('includes/header.php'); ?>

						<p class="outer">
						<!-- <h3 class="pull-left">IRM Open Programs</h3> -->
							<!-- <hr> -->

							
							<div class="col-md-11 col-sm-12 col-xs-12">
								<h5>What happened to Nokia?</h5>
								<h5>Why did Kingfisher Airlines collapse?</h5>
								<h5>Where is Blackberry?</h5>
								<h5>Do you see those old Kodak cameras today?</h5>	

								<div class="content_block">
									<p>
									A study reveals that <strong>75%</strong> of the companies that existed in <strong>1950</strong> will no longer exist as on <strong>2025</strong> - this highlights the increasing role of understanding risk management from a business perspective. IRM's Young Leaders Program on Business Risk Management is a two day practical and simulation based course in enterprise risk management that will allow you to learn tools and techniques that can be immediately applied at work, in family business or in your startup. It covers the fundamentals of enterprise risk management in a dynamic and interactive learning environment, using the theory and practice of risk management in-line with international standards and industry best practices. Risk is no longer about insurance, it's about taking decisions, mitigation strategies and role of stakeholders in managing risks. Candidates, upon completing the assessment test, would be awarded with a Certificate from IRM London.
									</p>	
								</div>
														
							</div>
							<h5>Upcoming Batch:</h5>	
							<hr>
							<div class="col-md-12 col-sm-12 col-xs-12">
								<div class="table-responsive table_reg">
									<table class="table">
										<tr>
											<td> <i class="fa fa-calendar-alt calener_icon"></i> 2nd and 3rd May 2019 <br><br> <span class="full_day">Full days</span></td>
											
											<td> <span class="seats">30 seats</span> <br><br> Registration on first come first serve	</td>

											<td>INR 16,500 plus taxes per student <br> [lunch, refreshments and snacks included]	</td>

											<td>Board Room at ITI EdVest Corporate Office</td>
										</tr>

										<tr>
											<td><i class="fa fa-calendar-alt calener_icon"></i>  18th and 19th May 2019<br><br>
											<span class="full_day">Full days</span>	</td>

											<td><span class="seats">30 seats</span> <br><br>
											Registration on first come first serve</td>

											<td>INR 16,500 plus taxes per student <br> [lunch, refreshments and snacks included]	</td>

											<td>Board Room at ITI EdVest Corporate Office</td>

										</tr>
									</table>
								</div>
							</div>

					
			            
						<div class="clearfix"></div>

							
						<div class="col-md-12 col-sm-12 col-xs-12">
							<p>	For inquiries contact: <a href="tel:02224391266">+91-22-24391266</a> or write to <a href="mailto:irm.edvest@itiorg.com">irm.edvest@itiorg.com</a>
							</p>						

							<p>Registration form: <a href="https://docs.google.com/forms/d/e/1FAIpQLSdlK710Uw-_c5pWG7y9mr28iKuRthbTdXB3WP56hXjjg0Ia5Q/viewform">https://goo.gl/forms/JVM2oARMaRYUGd842</a></p>
						</div>
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>
</body>
</html>
