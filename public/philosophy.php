<html>
<head>
	<title>Philosophy | ITI EdVest</title>
	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">


  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	
	<div class="fluid-container inner philosophy">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-6 padding-zero image-outer float-left">
						<img src="images/banners/philosophy.jpg" style="width:100%;">
					</div>

					<div class="col-md-6 text">
						<?php include_once('includes/header.php'); ?>
						
						<p class="outer">
							<h3 class="pull-left">Philosophy</h3>
							<hr>

							<ul class="philo_text">
								<li>We approach your problems in an innovative and dynamic yet professional manner</li>

								<li>We support our partners with speed, quality and transparency</li>

						        <li>We are continuously investing in technology, design thinking and process improvement to reduce turnaround time in delivering solutions</li>
							</ul>
							<img src="images/philosophy_text.jpg" class="philo_img">
						</p>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>

</body>
</html>