<html>
<head>
	<title>Social Impact | ITI EdVest</title>
  	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
  	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">
  	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/lightgallery/latest/css/lightgallery.css">

  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>

	<style type="text/css">
		.social .text{
			/*height: auto !important;*/
		}
		.pull-right{
			float: right;
		}
		.photo_section .img-responsive{
			    width: 100%;
		}
		.photo_section .img-responsive:hover{
		    -webkit-box-shadow: 4px 4px 12px 4px #9b9b9b;
		    box-shadow: 4px 4px 12px 4px #9b9b9b;
		    -webkit-transition: all 500ms ease-in-out;
		    -moz-transition: all 500ms ease-in-out;
		    -ms-transition: all 500ms ease-in-out;
		    -o-transition: all 500ms ease-in-out;
		    transition: all 500ms ease-in-out;
		}
		.photo_section li{
			    list-style: none;
		}
		.demo-gallery > ul > li a .demo-gallery-poster {
		    background-color: rgba(0, 0, 0, 0.1);
		    bottom: 0;
		    left: 0;
		    position: absolute;
		    right: 0;
		    top: 0;
		    -webkit-transition: background-color 0.15s ease 0s;
		    -o-transition: background-color 0.15s ease 0s;
		    transition: background-color 0.15s ease 0s;
		    display: none;
		}
		.photo_section #lightgallery li.col-md-4.col-xs-6.pull-right {
		    padding-bottom: 10px;
		    padding-left: 0px;
		}
	</style>
</head>
<body>
	
	<div class="fluid-container inner social">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-6 padding-zero image-outer float-left">
						<img src="images/banners/social.jpg" style="width:100%;">
					</div>

					<div class="col-md-6 text">
						<?php include_once('includes/header.php'); ?>
						
						<p class="outer">
							<h3 class="pull-left">Social Impact</h3>
							<hr>

              				<p class="">Krishna Vrundavan Pratishthan is a not for profit organisation run by the Promoter Group with special focus on enhancing quality education in tribal schools. We support such schools through technology and teacher training thereby investing in underprivileged students for their bright careers. If you wish to partner with us, please write to <a href="mailto:edvest@itiorg.com">edvest@itiorg.com</a></p>
							
						</p>

						<div class="photo_section">
						<div class="demo-gallery signign-mou">
						    <ul id="lightgallery">
						      <li class="col-md-4 col-xs-6 pull-right" data-responsive="images/social1.jpg" data-src="images/social1.jpg">
						        <a href="">
						          <img class="img-responsive" src="images/social1.jpg">
						          <div class="demo-gallery-poster">
						            <img src="https://sachinchoolur.github.io/lightGallery/static/img/zoom.png">
						          </div>
						        </a>
						      </li>
						      <li class="col-md-4 col-xs-6 pull-right" data-responsive="images/social2.jpg" data-src="images/social2.jpg">
						        <a href="">
						          <img class="img-responsive" src="images/social2.jpg">
						          <div class="demo-gallery-poster">
						            <img src="https://sachinchoolur.github.io/lightGallery/static/img/zoom.png">
						          </div>
						        </a>
						      </li>
						      <li class="col-md-4 col-xs-6 pull-right" data-responsive="images/social3.jpg" data-src="images/social3.jpg">
						        <a href="">
						          <img class="img-responsive" src="images/social3.jpg">
						          <div class="demo-gallery-poster">
						            <img src="https://sachinchoolur.github.io/lightGallery/static/img/zoom.png">
						          </div>
						        </a>
						      </li>
						      
						    </ul>
						  </div>
					</div>					
					</div>


					


				</div>
				<div class="clearfix"></div>
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="https://cdn.jsdelivr.net/g/lightgallery@1.3.5,lg-fullscreen@1.0.1,lg-hash@1.0.1,lg-pager@1.0.1,lg-share@1.0.1,lg-thumbnail@1.0.1,lg-video@1.0.1,lg-autoplay@1.0.1,lg-zoom@1.0.3"></script>
	
	<script src="inner.js"></script>

	<script type="text/javascript">
		$(document).ready(function() {
		  $('#lightgallery, #lightgallery1, #lightgallery2').lightGallery({
		    pager: true
		  });
		});
	</script>


</body>
</html>