<html>
<head>
	<title>What We Do | ITI EdVest</title>
	<!-- <link rel="shortcut icon" href="public/imgs/favicon.ico"> -->
	<meta name="description" content="An edu-focused initiative by Fortune Financial">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">
  	<link rel="stylesheet" type="text/css" href="public/scripts/inner.css">
  	<link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

  	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.bundle.min.js"></script>
</head>
<body>
	
	<div class="fluid-container inner what-we-do">
		<div class="col-md-12 padding-zero">
			<!--<div class="col-md-1 about">
				<img src="images/about-bar.jpg">
				<span class="home">
					<a href="index.html"><img src="images/home_icons/home.png"></a>
				</span>
			</div>-->

			<div class="col-md-12 padding-zero float-right">
				<div class="col-md-12 padding-zero">
					<div class="col-md-6 padding-zero image-outer float-left">
						<img src="images/banners/what_we_do.jpg" style="width:100%;">
					</div>

					<div class="col-md-6 text">
						<?php include_once('includes/header.php'); ?>
						
						<p class="outer">

							<h3 class="" style="text-align:left;">What We Do</h3>
							<hr>
							<div class="tagline">
								<p><sup><i class="fa fa-quote-left" aria-hidden="true" style="font-size: 10px"></i></sup> Give Wings To Your Education Institutions - ITI EdVest is catalyzing growth through structured finance and education services <sup><i class="fa fa-quote-right" aria-hidden="true" style="font-size: 10px"></i></sup></p>
							</div>
							
							<div class="top_what_we">
								<h5 class="title_tagline">Structured Finance [Fortune Integrated Assets Finance Limited]</h5>
								<!-- <h5 style="margin:30px 0;clear: both;"><li>Structured Finance [Fortune Integrated Assets Finance Limited]</li></h5> -->
								<p>Our group NBFC provides secured and unsecured finance for :</p>
															
							</div>
							<div class="row">
								<ul class="icon_outer">
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text"><span class="fin_text">Expansion / renovation of your campus</span></div>
										
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text"><span class="fin_text">Launching new courses</span></div>
										
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Improving your working capital</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Refinancing of existing debt</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Investing in technology & research</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Offering easy pay schemes to your students</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text">	<span class="fin_text">Setting up incubation centers, computer labs and other projects</span></div>
									
									</li>
									<li class="icon_inner">
										<div class="fin_icon">
											<img src="images/home_icons/group.png">
										</div>
										<div class="fin_text"><span class="fin_text">Supporting teachers in fulfilling their professional aspirations</span></div>
										
									</li>
								</ul>
							</div>
							<div class="top_what_we">
								<h5 class="title_tagline">Education Services [The Investment Trust Of India Limited] </h5>

								<!-- <h5 style="margin:30px 0;clear: both;"><li>Education Services [The Investment Trust Of India Limited]</li></h5> -->

								<p>ITI Limited provides globally certified proprietary simulation games focused on niche topics of relevance to students of premier education institutions in India. Our objective is to elevate these institutions by internationalization of their campuses. All our simulation games are spread over a period of 2 – 7 days with support from Indian and Global experts. Improve your accreditation and partner with us by writing to education@itiorg.com.</p>

							</div>

							<h5 style="margin:30px 0;clear: both;"><li>FUND SUPPORT</li></h5>

							<!--Accordion wrapper-->
							<div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingOne">
							            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							                <h5 class="mb-0">
							                    Want to infuse capital to boost your liquidity and fund your expansion plans? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx">
							            <div class="card-body">
							                Our group NBFC can structure secured and unsecured products that suit your cash flow
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->
							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingTwo">
							            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
							                <h5 class="mb-0">
							                    Want to grow through private equity fund raising or a merger/ acquisition or an IPO? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx">
							            <div class="card-body">
							                Our group investment banking team will help you in choosing your strategic partner
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->	

							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingTwo">
							            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapseThree" aria-expanded="false" aria-controls="collapseTwo">
							                <h5 class="mb-0">
							                    Want us to nurture entrepreneurial ventures run by your students? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx">
							            <div class="card-body">
							               Our group Alternative Investment Fund can be the apt platform to scale up these ventures through funding and support
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->	


							    <h5 style="margin:30px 0;"><li>ECOSYSTEM ENABLERS</li></h5>
							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingOne">
							            <a data-toggle="collapse" data-parent="#accordionEx" href="#collapse3" aria-expanded="true" aria-controls="collapseOne">
							                <h5 class="mb-0">
							                    Want to secure your institution and its property / keyman against any potential risk? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapse3" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx">
							            <div class="card-body">
							                Our group insurance team of risk consultants will support you in mitigating risks
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->
							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingTwo">
							            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse4" aria-expanded="false" aria-controls="collapseTwo">
							                <h5 class="mb-0">
							                    Want hostel accommodation for your students? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapse4" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx">
							            <div class="card-body">
							                Our real estate partner will provide necessary facilities either on campus or off campus
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->	

							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingTwo">
							            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse5" aria-expanded="false" aria-controls="collapseTwo">
							                <h5 class="mb-0">
							                    Want to integrate your processes seamlessly and leverage on technology? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapse5" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx">
							            <div class="card-body">
							                Our team of technology consultants will build the right platform for your institution
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->	
							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingTwo">
							            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse6" aria-expanded="false" aria-controls="collapseTwo">
							                <h5 class="mb-0">
							                    Want support in organizing guest speakers or immersion trips for students? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapse6" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx">
							            <div class="card-body">
							                Our team of expert speakers and consultants will help in transforming your experiences
							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->

							    <!-- Accordion card -->
							    <div class="card">
							        <!-- Card header -->
							        <div class="card-header" role="tab" id="headingTwo">
							            <a class="collapsed" data-toggle="collapse" data-parent="#accordionEx" href="#collapse7" aria-expanded="false" aria-controls="collapseTwo">
							                <h5 class="mb-0">
							                    Want your students to experience hassle free travel during the course of their study? <i class="fa fa-angle-down rotate-icon"></i>
							                </h5>
							            </a>
							        </div>
							        <!-- Card body -->
							        <div id="collapse7" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordionEx">
							            <div class="card-body">
							                Our group NBFC will provide easy finance schemes for their transport needs


							            </div>
							        </div>
							    </div>
							    <!-- Accordion card -->

							</div>
							<!--/.Accordion wrapper-->

							

							
							<!-- <ul>
								<li>Want to infuse capital to boost your liquidity and fund your expansion plans? - our group NBFC can structure secured and unsecured products that suit your cash flow</li>

								<li>Want to grow through private equity fund raising or a merger/ acquisition or an IPO? - our group investment banking team will help you in choosing your strategic partner</li>
							</ul> -->
						</p>

						<!--<p>
							<h5>ACADEMIC & ADMINISTRATIVE LEVERS</h5>
							<ul>
								<li>Want to secure your institution and its property / keyman against any potential risk? – our group insurance team of risk consultants will support you in mitigating risks</li>

								<li>Want to build new campuses? – our construction partner will provide the right platform for capacity expansion</li>

								<li>Want to integrate your processes seamlessly and leverage on technology? – our team of technology consultants will build the right platform for your institution</li>
							</ul>
						</p>-->

						<p>
							<!-- <h5>ECOSYSTEM ENABLERS</h5> -->

							<!--Accordion wrapper-->
							<!-- <div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
							    
							</div> -->
							<!--/.Accordion wrapper-->
							<!-- <ul>
								<li>Want to secure your institution and its property / keyman against any potential risk? - our group insurance team of risk consultants will support you in mitigating risks</li>

								<li>Want hostel accommodation for your students? - our real estate partner will provide necessary facilities either on campus or off campus</li>

								<li>Want to integrate your processes seamlessly and leverage on technology? - our team of technology consultants will build the right platform for your institution</li>

								<li>Want support in organizing guest speakers or immersion trips for students? - our team of expert speakers and consultants will help in transforming your experiences</li>
							</ul> -->
						</p>
					</div>
				</div>
				<div class="clearfix"></div>
				
				<?php include_once('includes/footer.php'); ?>

			</div>
		</div>
	</div>

	<script src="inner.js"></script>


</body>
</html>

