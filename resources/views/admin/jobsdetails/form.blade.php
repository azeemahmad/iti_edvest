<div class="panel-group">
    <div class="panel panel-default  panel-primary">
        <div class="panel-heading"><b>Jobsdetail</b></div>
        <br/>
         <div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    <label for="title" class="col-md-2 control-label">{{ 'Title' }} :</label>
    <div class="col-md-8">
     <input class="form-control" name="title" type="text" id="title" value="{{ isset($jobsdetail->title) ? $jobsdetail->title : ''}}" required>
    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('descriptions') ? 'has-error' : ''}}">
    <label for="descriptions" class="col-md-2 control-label">{{ 'Descriptions' }} :</label>
    <div class="col-md-8">
     <textarea class="form-control ckeditor" rows="5" name="descriptions" type="textarea" id="descriptions" required>{{ isset($jobsdetail->descriptions) ? $jobsdetail->descriptions : ''}}</textarea>
    {!! $errors->first('descriptions', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('type') ? 'has-error' : ''}}">
    <label for="type" class="col-md-2 control-label">{{ 'Type' }} :</label>
    <div class="col-md-8">
     <select name="type" class="form-control" id="type" required>
    @foreach (json_decode('{"1": "Full Time", "2": "Part Time"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($jobsdetail->type) && $jobsdetail->type == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('experience') ? 'has-error' : ''}}">
    <label for="experience" class="col-md-2 control-label">{{ 'Experience' }} :</label>
    <div class="col-md-8">
     <select name="experience" class="form-control" id="experience" required>
    @foreach ($experience as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($jobsdetail->experience) && $jobsdetail->experience == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('experience', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('department') ? 'has-error' : ''}}">
    <label for="department" class="col-md-2 control-label">{{ 'Department' }} :</label>
    <div class="col-md-8">
     <select name="department" class="form-control" id="department" required>
    @foreach ($department as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($jobsdetail->department) && $jobsdetail->department == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('department', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('city') ? 'has-error' : ''}}">
    <label for="city" class="col-md-2 control-label">{{ 'City' }} :</label>
    <div class="col-md-8">
     <select name="city" class="form-control" id="city" required>
    @foreach ( $city as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($jobsdetail->city) && $jobsdetail->city == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('city', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    <label for="status" class="col-md-2 control-label">{{ 'Status' }} :</label>
    <div class="col-md-8">
     <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($jobsdetail->status) && $jobsdetail->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
    {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
          <div class="form-group">
                <div class="col-md-offset-4 col-md-6">
                      <input class="btn btn-primary" type="submit" value="{{ $formMode === 'edit' ? 'Update' : 'Create' }}">
                </div>
          </div>
     </div>
</div>
