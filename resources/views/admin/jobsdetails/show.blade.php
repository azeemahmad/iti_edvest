@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9">
                <div class="panel panel-success">
                    <div class="panel-heading">Jobsdetail {{ $jobsdetail->id }}</div>
                    <div class="panel-body">

                        <a href="{{ url('/admin/jobsdetails') }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <a href="{{ url('/admin/jobsdetails/' . $jobsdetail->id . '/edit') }}" title="Edit Jobsdetail">
                            <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"
                                                                      aria-hidden="true"></i> Edit
                            </button>
                        </a>

                        <form method="POST" action="{{ url('admin/jobsdetails' . '/' . $jobsdetail->id) }}"
                              accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-xs" title="Delete Jobsdetail"
                                    onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o"
                                                                                             aria-hidden="true"></i>
                                Delete
                            </button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="data-table">
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <td>{{ $jobsdetail->id }}</td>
                                </tr>
                                <tr>
                                    <th> Title</th>
                                    <td> {{ $jobsdetail->title }} </td>
                                </tr>
                                <tr>
                                    <th> Descriptions</th>
                                    <td width="650px"> {!! $jobsdetail->descriptions !!} </td>
                                </tr>
                                <tr>
                                    <th> Type</th>
                                    <td> {{ $jobsdetail->type==1?'Full Time':'Part Time' }} </td>
                                </tr>
                                <tr>
                                    <th> Experience</th>
                                    <td> {{ isset($jobsdetail->experiences->experience)?$jobsdetail->experiences->experience:'' }} </td>
                                </tr>
                                <tr>
                                    <th> Department</th>
                                    <td> {{ isset($jobsdetail->departments->name)?$jobsdetail->departments->name:'' }} </td>
                                </tr>
                                <tr>
                                    <th> City</th>
                                    <td> {{ isset($jobsdetail->cities->name)?$jobsdetail->cities->name:'' }} </td>
                                </tr>
                                <tr>
                                    <th> Status</th>
                                    <td> @if($jobsdetail->status==1) <span style="color:green">Enabled</span> @else <span style="color:red">Disabled</span> @endif </td>
                                </tr>
                                <tr>
                                    <th> Job Created Date</th>
                                    <td> {{date('d M Y',strtotime($jobsdetail->created_at))}} </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
